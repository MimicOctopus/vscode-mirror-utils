#!/usr/bin/env python3

"""
A command line wrapper to dump/mirror the current VSCode marketplace
"""

import argparse
import sys
from vscode.api import download_vsix, get_new_index, search_index, build_mongo_string

# Obviously, fix this eventually
DEV_MONGO_CONN = build_mongo_string("localhost", 27017)


class CliRunner(object):

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Cli to handle Microsoft VSCode plugin mirroring ',
            usage='''vscode_cli <command> [<args>]''')
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        # use dispatch pattern to invoke method with same name
        getattr(self, args.command)()

    def update(self):
        """
        Download all new extensions to backing storage
        """
        for ext in search_index(".*", DEV_MONGO_CONN):
            download_vsix(ext)

    def serve(self):
        parser = argparse.ArgumentParser(
            description='Serves the artifacts so that they are available')
        # NOT prefixing the argument with -- means it's not optional
        parser.add_argument('--prod')
        parser.add_argument('--ip', type=str, default="*")
        parser.add_argument('--port', type=int, default="3000")
        args = parser.parse_args(sys.argv[2:])
        raise NotImplementedError("Need to finish this")

    def clean(self):
        """
        Clean old packages out
        """
        raise NotImplementedError()

    def index(self):
        """
        Create a new index
        """
        get_new_index(DEV_MONGO_CONN)


def main():
    """
    The test main function
    """
    CliRunner()


if __name__ == "__main__":
    main()
