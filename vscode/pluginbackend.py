from enum import Enum, IntEnum
from json import dumps
from os.path import isfile, join, dirname
from typing import Any, Dict, Generator
import requests
import re
from pathlib import Path

"""
An abstraction over the MS VSCode plugin backend API
"""

DEFAULT_TARGET = "VSCode"
PACKAGE_RESOURCE = "Microsoft.VisualStudio.Services.VSIXPackage"
SEARCH_BASE_URL2 = "https://marketplace.visualstudio.com/_apis/public/gallery/extensionquery"
# Things that the microsoft API wants
SEARCH_HEADERS = {"Accept": "application/json;api-version=4.0-preview.1;excludeUrls=true",
                  "Content-Type": "application/json"}


class FilterTypes(IntEnum):
    """
    FilterTypes enum from the VSCode Market API
    """
    package = 8
    search_term = 10
    category = 12


class Category(Enum):
    """
    Category enum from the VSCode Market API
    """
    all = "5122"


class SortBy(IntEnum):
    """
    SortBy enum from the VSCode Market API
    """
    relevance = 0
    updated = 1


def build_query(search_term: str,
                category: Category = Category.all,
                sort_by: SortBy = SortBy.relevance) -> Dict[str, Any]:
    """
    Build a query from the provided parameters
    :param search_term: A search term we want to find extensions for
    :param category: A category number
    :param sort_by: What type of sorting we want on this query
    :return: the json representation of the query
    """
    query = {"filters": [
        {
            "criteria": [
                {
                    "filterType": FilterTypes.package,
                    "value": "Microsoft.VisualStudio.Code",
                },
                {
                    "filterType": FilterTypes.category,
                    "value": str(category)
                },
            ],
            "direction": 2,
            "pageNumber": 1,
            "sortBy": sort_by,
            "pagingToken": None
        }
    ],
             "flags": 870
            }
    if search_term:
        criteria = {"filterType": FilterTypes.search_term,
                    "value": search_term}
        query['filters'][0]['criteria'].append(criteria)

    return query


class Publisher(object):
    """
    Representation of the VSCode Marketplace API Publisher information
    """
    def __init__(self, publisher_id: str, publisher_name: str, display_name: str, flags: str):
        self.publisher_id = publisher_id
        self.publisher_name = publisher_name
        self.display_name = display_name
        self.flags = flags

    @staticmethod
    def from_dict(publisher: Dict[str, str]) -> 'Publisher':
        """
        Create a new Publisher from the given dictionary
        """
        return Publisher(publisher['publisherId'],
                         publisher['publisherName'],
                         publisher['displayName'],
                         publisher['flags'])


class Version(object):
    """
    The VSCode Market API version information
    """
    def __init__(self, version: str, flags: str, last_updated: str, asset_uri: str) -> None:
        self.version = version
        self.flags = flags
        self.last_updated = last_updated
        self.asset_uri = asset_uri

    @staticmethod
    def from_dict(version: Dict[str, str]) -> 'Version':
        """
        Create a new Version from the provided dictionary
        """
        return Version(version['version'],
                       version['flags'],
                       version['lastUpdated'],
                       version['assetUri'])


class Extension(object):
    """
    A representation of a VSCode extension
    """
    def __init__(self, extension_id: str, extension_name: str,
                 display_name: str, flags: str, last_updated: str,
                 published_date: str, release_date: str,
                 short_description: str, publisher: Publisher,
                 versions: [Version]) -> None:
        self.extension_id = extension_id
        self.extension_name = extension_name
        self.display_name = display_name
        self.flags = flags
        self.last_updated = last_updated
        self.published_date = published_date
        self.release_date = release_date
        self.short_description = short_description
        self.publisher = publisher
        self.versions = versions

    @staticmethod
    def from_dict(extension: Dict[str, Any]) -> 'Extension':
        """
        Create an extension from a dictionary
        :param extension: The dictionary representation the extension
        """
        versions = []
        for version in extension['versions']:
            versions.append(Version.from_dict(version))
        short_description = ""
        if 'shortDescription' in extension:
            short_description = extension['shortDescription']
        return Extension(extension['extensionId'],
                         extension['extensionName'],
                         extension['displayName'],
                         extension['flags'],
                         extension['lastUpdated'],
                         extension['publishedDate'],
                         extension['releaseDate'],
                         short_description,
                         Publisher.from_dict(extension['publisher']),
                         versions)

    def get_vsix_url(self, version: str = "") -> str:
        """
        Get the VSIX for this extension
        By default the most recent version is fetched
        :param version: An optional version number that will be prefered over the most recent one
        """
        uri = self.versions[0].asset_uri
        if version:
            for ver in self.versions:
                if ver.version == version:
                    uri = ver.asset_uri
        return "{}/{}".format(uri, PACKAGE_RESOURCE)

    def get_vsix_filename(self, version: str = "") -> str:
        """
        Generate a VSIX filename for this extension
        By default we generate a filename for the most current version
        :param version: An optional version number
        """
        if not version:
            version = self.versions[0].version

        return "{}_{}_{}.vsix".format(self.publisher.publisher_name, self.extension_name, version)

def get_index(query=""):
    """
    Gets a new index by querying the MS server for everything
    """
    extension_count = 0
    query = build_query(query, sort_by=SortBy.updated)

    while 1:
        req = requests.post(SEARCH_BASE_URL2, data=dumps(query), headers=SEARCH_HEADERS)
        results = req.json()['results'][0]
        extension_count += len(results['extensions'])
        for ext in results['extensions']:
            yield ext
        total_count = {}
        for result_meta in results['resultMetadata']:
            for meta in result_meta['metadataItems']:
                if 'name' in meta and meta['name'] == "TotalCount":
                    total_count = meta
                    break
        if extension_count >= total_count['count']:
            print("Total extensions: {}".format(extension_count))
            break
        query['filters'][0]['pageNumber'] += 1

def recent_updates(num_days):
    from datetime import datetime, timezone, timedelta
    from dateutil import parser
    now = datetime.now(timezone.utc)
    today = datetime(now.year, now.month, now.day, tzinfo=timezone.utc)
    d = today - timedelta(days=num_days)
    for ext in get_index():
        if parser.parse(ext['lastUpdated']) >= d:
            yield ext
        else: # Sorting by update date, so start dropping if the field goes missing
            break
