from os.path import isfile, join, dirname
from pathlib import Path
import requests
import toml
import vscode.pluginbackend as vscoderss

def download_file(src, dst="", basedir=""):
    try:
        with requests.get(src, stream=True) as request:
            if not dst:
                dst = request.headers.get("Content-Disposition").split("filename=")[1].replace("\"", "")
            dst = join(basedir, dst)
            Path(dirname(dst)).mkdir(parents=True, exist_ok=True)
            if isfile(dst):
                print("Already got `{}`".format(dst))
                return
            with open(dst, 'wb') as output_file:
                for chunk in request.iter_content(chunk_size=2048):
                    output_file.write(chunk)
    except requests.exceptions.SSLError:
        print("Failure getting: `{}`".format(src))
    

def download_vsix(extension: vscoderss.Extension) -> None:
    """
    Download the vsix for the given extension
    Currently $(pwd)/plugins
    """
    # We might want to mirror to an S3 bucket or something
    # We just want to easily get a big tarball we can get from "the cloud"
    download_file(extension.get_vsix_url(), dst=extension.get_vsix_filename(), basedir="plugins")

def get_plugins(requested_plugins):
	for request in requested_plugins:
		for i in vscoderss.get_index(request):
			ext = vscoderss.Extension.from_dict(i)
			print(ext.get_vsix_url())
			print(ext.get_vsix_filename())
			download_vsix(ext)

def get_extra_packages(requested):
    for plugin in requested:
        download_file(plugin, basedir="plugins")

def get_vscode(requested):
    for installer in requested:
        download_file(installer, basedir="installers")

def plugins_from_config(config):
    for group in config['plugins']:
        for x in config['plugins'][group]:
            yield x

def extras_from_config(config):
    for entry in config['extra_packages']:
        yield config['extra_packages'][entry]

def installers_from_config(config):
    for entry in config['vscode']:
        yield   config['vscode'][entry]
    for entry in config['code-server']:
        yield   config['code-server'][entry]

def main():
    config = toml.load("config.toml")
    get_plugins(plugins_from_config(config))
    get_extra_packages(extras_from_config(config))
    get_vscode(installers_from_config(config))

if __name__=="__main__":
    main()