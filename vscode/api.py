from enum import Enum, IntEnum
from json import dumps
from os.path import isfile, join, dirname
from typing import Any, Dict, Generator
import requests
from pymongo import MongoClient
import re
from pathlib import Path

from vscode.pluginbackend import *

def build_mongo_string(host: str, port: int) -> str:
    """
    Build a mongo connection string from the given host and port
    """
    return 'mongodb://{}:{}/'.format(host, port)





def get_new_index(db_addr: str):
    """
    Gets a new index by querying the MS server for everything
    """
    extension_count = 0
    client = MongoClient(db_addr)
    db = client.vscode
    exts = db.extensions
    # The query should probably be an object or something with appropriate methods
    query = build_query("")

    while 1:
        req = requests.post(SEARCH_BASE_URL2, data=dumps(query), headers=SEARCH_HEADERS)
        results = req.json()['results'][0]
        # exts.insert_many(results['extensions'])
        for ext in results['extensions']:
            exts.replace_one({"extensionId": ext['extensionId']}, ext, True)
        extension_count += len(results['extensions'])
        total_count = {}
        for result_meta in results['resultMetadata']:
            for meta in result_meta['metadataItems']:
                if 'name' in meta and meta['name'] == "TotalCount":
                    total_count = meta
                    break
        if extension_count >= total_count['count']:
            print("Total extensions: {}".format(extension_count))
            break
        query['filters'][0]['pageNumber'] += 1


def download_vsix(extension: Extension) -> None:
    """
    Download the vsix for the given extension
    Currently $(pwd)/plugins
    """
    # We might want to mirror to an S3 bucket or something
    # We just want to easily get a big tarball we can get from "the cloud"
    vsix_file_path = join("plugins", extension.get_vsix_filename())
    Path(dirname(vsix_file_path)).mkdir(parents=True, exist_ok=True)
    if isfile(vsix_file_path):
        print("Already got `{}`".format(extension.get_vsix_filename()))
        return
    try:
        request = requests.get(extension.get_vsix_url())
    except requests.exceptions.SSLError:
        print("Failure getting: `{}`".format(extension.get_vsix_url()))
        return
    with open(vsix_file_path, 'wb') as output_file:
        for chunk in request.iter_content(chunk_size=2048):
            output_file.write(chunk)


def search_index(search_term: str, db_addr: str) -> Generator[None, Extension, None]:
    """
    Searches the local index
    """
    client = MongoClient(db_addr)
    db = client.vscode
    exts = db.extensions
    reg = re.compile(search_term, re.IGNORECASE)
    for ext in exts.find({"extensionName": reg}):
        yield Extension.from_dict(ext)
    return None

