import asyncio
import uvloop
from feedgen.feed import FeedGenerator
from sanic import Sanic
from sanic import response
from dateutil import parser

from vscode.pluginbackend import Extension, recent_updates

class UpdateFeed():
    def __init__(self, title, description, link):
        self.title = title
        self.description = description
        self.link = link

    async def get_recent(self, request):
        fg = FeedGenerator()
        fg.title(self.title)
        fg.link(href=self.link)
        fg.description(self.description)
        fg.language("en")
        for entry in recent_updates(7):
            ext = Extension.from_dict(entry)
            fe = fg.add_entry()
            fe.guid(ext.get_vsix_url(), permalink=True)
            fe.title(ext.display_name)
            fe.link(href=ext.get_vsix_url())
            fe.content(content=ext.short_description)
            fe.pubDate(parser.parse(ext.last_updated))
        return response.text(fg.rss_str(pretty=True).decode("utf8"), content_type="application/rss+xml")

def main():
    app = Sanic(__name__)
    # Ah! uvloop goes brrr
    uvloop.install()
    title = "Recently updated VSCode plugins"
    description = "A feed of recently updated VSCode plugins"
    link = "https://vscode.nullptrsolutions.ca"
    uf = UpdateFeed(title, description, link)
    app.add_route(uf.get_recent, "/feed.rss")

    # Prep
    loop = asyncio.get_event_loop()
    # Run
    server = app.create_server(host="0.0.0.0", port=8000, debug=False, return_asyncio_server=True)
    task = asyncio.ensure_future(server)
    loop.run_forever()

if __name__ == "__main__":
    main()
