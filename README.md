# VSCode Mirroring Utilities

These are utilities built in spare time to help with the mirroring of VSCode plugins for offline environments
They were originally built in a couple days by reading VSCode documentation and poking around the API (responsibly)

## Where is it now?

I've got a basic understanding of the API going, and I can create a "dump" of all plugins at a specific point in time
Using this dump in an offline environment currently requires building a tarball and manually installing in the offline environment, which is far from convinient
I've been working on building a proper index, which brings me to...

## Where are we going?

Eventually I'd like this project to include the capability to host a mirror.
Best case, it perfectly mimics the API the editor expects and can be used semi-transparently
If I can provide a basic query GUI so that you can search plugins from the offline environment, I'd probably be happy

Of course there's still a bunch of experimental code in here, so please use responsibly and at your own risk.
