
FROM python:3-slim-buster

COPY requirements.txt /build/
RUN apt update && apt install libxml2-dev libxslt-dev build-essential zlib1g-dev && apt clean && pip3 install -r /build/requirements.txt
COPY dist/*.whl /build/
RUN pip3 install /build/vscode*.whl
WORKDIR /config
CMD vscode-rss
