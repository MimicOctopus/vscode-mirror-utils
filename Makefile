all:
	poetry build
	poetry export -f requirements.txt > requirements.txt
	podman build --rm -t vscode-rss .
